import menuFc from "./menu/index.js";
import action from "./action/index.js";

// 右键菜单相关
menuFc();

// Action
action();

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  // 接受消息
  if (request.type === "PToB") {
    sendResponse({ message: "接受到了popup的弹框了，goodbye" });
  }else if(request.type === 'CToB'){
    sendResponse({ message: "%c 你好， contentscript，我是background！！！" });
  }
});

// 长期连接 监听
chrome.runtime.onConnect.addListener(function (port) {
  port.onMessage.addListener(function (msg) {
    if (port.name === "CToB")
      port.postMessage({ message: port.name + "，你好,我们可以开始通话了" });
  });
});
