// 修改 百度 改成 谷歌
// setTimeout(() => {
//   document.body.innerHTML = document.body.innerHTML.replace(/百度/g, "谷歌");
// }, 2000);
// 向页面注入JS
function injectCustomJs(jsPath) {
  jsPath = jsPath || "js/inject.js";
  var temp = document.createElement("script");
  temp.setAttribute("type", "text/javascript");
  // 获得的地址类似：chrome-extension://ihcokhadfjfchaeagdoclpnjdiokfakg/js/inject.js
  temp.src = chrome.runtime.getURL(jsPath);
  temp.onload = function () {
    // 放在页面不好看，执行完后移除掉
    this.parentNode.removeChild(this);
  };
  document.body.appendChild(temp);
}

injectCustomJs();

// 发送消息给background
chrome.runtime.sendMessage({ type: "CToB" }, function (response) {
  console.log(response.message, "color:red; font-size: 16px;");
});

// 接收来自popup的消息
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.type === "PToC")
    sendResponse("你好，我是contentscript，我收到了你的信息了");
});

// 长期连接监听
chrome.runtime.onConnect.addListener(function (port) {
  console.log(port);
  port.onMessage.addListener(function (msg) {
    if (port.name === "PToC")
      port.postMessage({
        message: port.name + "，你好,我是contentscript,我们可以开始通话了",
      });
  });
});
