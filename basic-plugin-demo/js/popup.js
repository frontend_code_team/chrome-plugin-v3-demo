function createQrcode(codeText) {
  //生成二维码
  var qrcode = $(".qrcode").qrcode({
    render: "canvas",
    text: codeText || location.href,
    width: "400", //二维码的宽度
    height: "400", //二维码的高度
    background: "#ffffff", //二维码的后景色
    foreground: "#000000", //二维码的前景色
    src: codeText || location.href, //二维码中间的图片
  });
  var canvas = qrcode.find("canvas").get(0);
  $(".qrcode-img").attr("src", canvas.toDataURL("image/png")).css({
    maxWidth: 300,
  });
}

function notice(title, message) {
  chrome.notifications.create(null, {
    type: "basic",
    iconUrl: "/img/logo.png",
    title: title || "你好呀",
    message: message || "人生短短几十年，何不疯狂装疯癫!",
  });
}

$(function () {
  let currentTabs = {};
  /**
   * 这里是管理tab选项卡 ，获取所有的tab信息
   * active: 选项卡在其窗口中是否处于活动状态
   * currentWindow: 选项卡是否在当前窗口中
   */
  chrome.tabs.query(
    { active: true, currentWindow: true },
    async function (tabs) {
      // 满足要求这里才会执行
      createQrcode(tabs[0].url);
      // 赋值当前 tab
      currentTabs = tabs[0];
    }
  );

  // 显示badge
  $("#showBadge").click(() => {
    // 官网说 空间中只能容纳大约四个字符   我的只能看到两个=====
    chrome.action.setBadgeText({
      text: "显示标识",
      tabId: currentTabs.id,
    });
    chrome.action.setBadgeBackgroundColor(
      { color: [0, 255, 0, 0] }, // Green
      () => {
        /* ... */
      }
    );
  });

  // 隐藏 badge
  $("#hideBadge").click(() => {
    // 重新设置    并没有特定属性重置
    chrome.action.setBadgeText({
      text: "",
      tabId: currentTabs.id,
    });
    chrome.action.setBadgeBackgroundColor({ color: [0, 0, 0, 0] });
  });

  // 设置ICON
  $("#showIcon").click(() => {
    chrome.action.setIcon({
      path: "/img/wer.png",
      tabId: currentTabs.id,
    });
  });

  // 还原ICON
  $("#hideIcon").click(() => {
    chrome.action.setIcon({
      path: "/img/logo.png",
      tabId: currentTabs.id,
    });
  });

  // 修改工具提示
  $("#tootipEdit").click(() => {
    chrome.action.setTitle({
      title: "修改工具提示成功",
      tabId: currentTabs.id,
    });

    notice("提示", "修改工具提示成功");
  });

  // 当前页面设置新的popup页面
  // 设置 HTML 文档在用户单击操作图标时作为弹出窗口打开。
  // popup隐藏后，再次点击选项卡才会显示设置的新的popup
  $("#openPopup").click(() => {
    chrome.action.setPopup({
      popup: "../html/subPopup.html",
      tabId: currentTabs.id, // 将更改限制在选择特定选项卡时。选项卡关闭时自动重置。
    });
  });

  // 禁用当前页面选项卡
  $("#disablePopup").click(() => {
    chrome.action.disable(currentTabs.id);
  });

  // 打开新窗口
  $("#openWindow").click(() => {
    chrome.windows.create({
      // focused: false, // 如果为 true，则打开一个活动窗口。如果为 false，则打开一个非活动窗
      // width: 400, //新窗口的宽度（以像素为单位），包括框架。如果未指定，则默认为自然宽度。
      // height: 400, // 新窗口的高度（以像素为单位），包括框架。如果未指定，则默认为自然高度。
      // state: "minimized", // 窗口的初始状态。minimized, maximized, 和 fullscreen不能与 left、top、width 或 height 组合。
      // incognito: false, // 新窗口是否应该是隐身窗口。
      // url: "http://www.baidu.com", // 要在窗口中作为选项卡打开的 URL 或 URL 数组。
    });
  });

  //最大化窗口
  $("#maxWindow").click(() => {
    chrome.windows.getCurrent({}, (currentWindow) => {
      chrome.windows.update(currentWindow.id, { state: "maximized" });
    });
  });

  // 最小化窗口
  $("#miniWindow").click(() => {
    chrome.windows.getCurrent({}, (currentWindow) => {
      chrome.windows.update(currentWindow.id, { state: "minimized" });
    });
  });

  // 全屏窗口
  $("#fullWindow").click(() => {
    chrome.windows.getCurrent({}, (currentWindow) => {
      chrome.windows.update(currentWindow.id, { state: "fullscreen" });
    });
  });

  // 删除window
  $("#delWindow").click(() => {
    chrome.windows.getCurrent({}, (currentWindow) => {
      chrome.windows.remove(currentWindow.id);
    });
  });

  // 打开新标签页面
  $("#newTab").click(() => {
    chrome.tabs.create({
      // index: 1, // 选项卡在窗口中应占据的位置。提供的值被限制在零和窗口中的选项卡数量之间。
      // url: "https://www.baidu.com",
    });
  });

  // 当前标签打开网页
  $("#tabOpenWeb").click(() => {
    chrome.tabs.update(currentTabs.id, { url: "https://www.baidu.com" });
  });

  // 高亮第一个标签
  $("#tabHighlight").click(() => {
    chrome.tabs.highlight({ tabs: 0 });
  });

  // 复制选项卡。
  $("#duplicateTab").click(() => {
    chrome.tabs.duplicate(currentTabs.id);
  });

  // 重新加载
  $("#reloadTab").click(() => {
    chrome.tabs.reload(currentTabs.id, {
      bypassCache: true, // 是否绕过本地缓存。默认为false。
    });
  });
  // 关闭标签页 （选项卡）
  $("#delTab").click(() => {
    //  获取当前窗口所有的tab   不加active
    chrome.tabs.query({ currentWindow: true }, async function (tabs) {
      chrome.tabs.remove([tabs[0]?.id, tabs[1].id]);
    });
  });

  // 发送消息到background   长连接  互通消息
  $("#PTOB1").click(() => {
    var port = chrome.runtime.connect({ name: "CToB" });
    port.postMessage({ message: "hello hello" });
    port.onMessage.addListener(function (msg) {
      alert(msg.message);
    });
  });

  $("#PTOB").click(() => {
    // 发送消息到background
    chrome.runtime.sendMessage({ type: "PToB" }, function (response) {
      alert(response.message);
    });
  });

  // popup发消息给contentscript
  $("#PTOC").click(() => {
    chrome.tabs.sendMessage(
      currentTabs.id,
      {
        type: "PToC",
        message: "你好，我来自popup！",
      },
      function (response) {
        alert("收到来自content-script的回复：" + response);
      }
    );
  });

  // popup与contentscript互通（长连接）
  $("#PTOC2").click(() => {
    // 连接到指定选项卡中的内容脚本
    var port = chrome.tabs.connect(currentTabs.id, { name: "PToC" });
    port.postMessage({ message: "hello hello" });
    port.onMessage.addListener(function (msg) {
      alert(msg.message);
    });
  });
});
