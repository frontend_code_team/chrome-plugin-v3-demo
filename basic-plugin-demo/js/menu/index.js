function linkOpenEvent(info, tab) {
  chrome.tabs.create({
    url: info.linkUrl || info.selectionText,
  });
}

function openNotice() {
  chrome.notifications.create(null, {
    type: "basic",
    iconUrl: "/img/logo.png",
    title: "你好呀",
    message: "人生短短几十年，何不疯狂装疯癫!",
  });
}

function baiduSearchEvent(info, tab) {
  chrome.tabs.create({
    url: `https://www.baidu.com/s?ie=utf-8&wd=${info.selectionText}`,
  });
}

export default function () {
  const idProps = {
    linkOpen: "link-open",
    baidu: "baidu-search",
    notice: "notice-open",
  };
  // 右键菜单------新标签页打开链接
  chrome.contextMenus.create({
    id: idProps.linkOpen,
    type: "normal", // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
    title: "新标签页打开链接", // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
    contexts: ["link"], // 上下文环境，可选：["all", "page", "frame", "selection", "link", "editable", "image", "video", "audio"]，默认page
  });

  // 右键菜单------百度搜索
  chrome.contextMenus.create({
    id: idProps.baidu,
    type: "normal",
    title: "百度搜索：%s",
    contexts: ["selection"],
  });

  // 右键菜单------打开通知框
  chrome.contextMenus.create({
    id: idProps.notice,
    type: "normal",
    title: "打开通知框",
    contexts: ["page"],
  });

  //   右键菜单点击监听事件
  chrome.contextMenus.onClicked.addListener((info, tab) => {
    switch (info.menuItemId) {
      case idProps.linkOpen:
        linkOpenEvent(info, tab);
        break;
      case idProps.baidu:
        baiduSearchEvent(info, tab);
        break;
      case idProps.notice:
        openNotice(info, tab);
        break;
    }
  });
}
